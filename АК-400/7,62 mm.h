#pragma once

template <class type_id> //������ (������� �� �������)
class Matrice
{

protected:
	type_id ** Array;
	int nstroki;
	int nstolb;

public:

	Matrice<type_id>() //�����������
	{
		** replica = { 0 };
		** Array = { 0 };
		nstroki = 0;
		nstolb = 0;
	}

	Matrice<type_id>(int row, int col) //�����������
	{
		nstroki = row;
		nstolb = col;
		replica = new type_id *[nstroki];
		Array = new type_id *[nstroki];

		for (int i = 0; i < nstroki; i++)
		{
			replica[i] = new type_id[nstolb];
			Array[i] = new type_id[nstolb];
		}
	}

	type_id ** replica;

	void write_Array() //���������� �������
	{
		for (int i = 0; i < nstroki; i++)
		{
			for (int j = 0; j < nstolb; j++)
			{
				Array[i][j] = (rand() % 100) / 10.0;
				replica[i][j] = Array[i][j];
			}
		}
	}

	void look_Array()//����� �������
	{
		for (int i = 0; i < nstroki; i++)
		{
			for (int j = 0; j < nstolb; j++)
			{
				std::cout << replica[i][j] << '\t';
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;
	}

	void look_Array_for_skal()//����� ������� ��� ���������� ������������ � ��������� ������� �� ������
	{
		for (int i = 0; i < nstroki; i++)
		{
			std::cout << replica[i][0] << std::endl;
		}

		std::cout << std::endl;
	}

	void add_array(type_id **& matr_a, type_id **& matr_b) //  �������� ������
	{
		for (int i = 0; i < nstroki; i++)
		{
			for (int j = 0; j < nstolb; j++)
			{
				Array[i][j] = 0;
				Array[i][j] += matr_a[i][j] + matr_b[i][j];
			}
		}
	}

	void sabtraction(type_id **& matr_a, type_id **& matr_b) //���������
	{
		for (int i = 0; i < nstroki; i++)
		{
			for (int j = 0; j < nstolb; j++)
			{
				Array[i][j] = 0;
				Array[i][j] += matr_a[i][j] - matr_b[i][j];
			}
		}
	}

	void mult_Array(type_id ** &matr_a, type_id ** &matr_b, int row_a, int col_a, int col_b) //��������� ������
	{
		for (int i = 0; i < row_a; i++)
		{
			for (int j = 0; j < col_b; j++)
			{
				Array[i][j] = 0;
				for (int k = 0; k < col_a; k++)
				{
					Array[i][j] += matr_a[i][k] * matr_b[k][j];
				}
			}
		}
	}

	void mult_Array2(type_id ** &vec_a, type_id ** &matr_b, int row, int col)// ��������� ������� �� ������
	{
		for (int i = 0; i < row; i++)
		{
			for (int j = 0; j < col; j++)
			{
				Array[i][j] = 0;
				Array[i][0] += matr_b[i][j] * vec_a[0][j];
			}
		}
	}

	void delete_Array() //�������� ������
	{
		for (int i = 0; i < nstroki; i++)
		{
			delete[] replica[i];
			delete[] Array[i];

		}
		delete[] replica;
		delete[] Array;
	}

	type_id* operator[](int n); //���������� [] (������� �� �������)
	
	~Matrice() //���������� 
	{

	}

};

template <class type_id> //������ (������� �� �������)
class Vector : public Matrice<type_id> // ������������
{
public:
	Vector() : Matrice() {};
	Vector(int col) : Matrice(1, col) {};

	type_id* operator[](int n); // ���������� []
	

	void mult_vec(type_id ** &vec1, type_id ** &vec2) // ��������� ���������
	{
		


		std::cout << std::endl;
		std::cout << std::endl;
		type_id rezult_vec[1][3];

		rezult_vec[0][0] = vec1[0][1] * vec2[0][2] - vec1[0][2] * vec2[0][1];//1-�� ���������� �������
		rezult_vec[0][1] = vec1[0][2] * vec2[0][0] - vec1[0][0] * vec2[0][2];//2-�� ���������� �������
		rezult_vec[0][2] = vec1[0][0] * vec2[0][1] - vec1[0][1] * vec2[0][0];//3-�� ���������� �������


		std::cout << "Vector Rez = " << std::endl;
		for (int i = 0; i < 1; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				std::cout << rezult_vec[i][j] << '\t';//����� ���������� ���������� ��������� ��������
			}

		}
	}

};


