// ConsoleApplication3.cpp: определяет точку входа для консольного приложения.
//
#include "stdafx.h"
#include <iostream>
// функция сложения матриц
void fnc_1(int matr1[3][3],
	       int matr2[3][3],
	       int (matr_sum)[3][3])
{
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			matr_sum[i][j] = matr1[i][j] + matr2[i][j];
		}
}

// функция сложения векторов
void fnc_2(int stol1[1][3],
           int stol2[1][3],
           int (&vector_sum)[1][3] )
{
	for (int i = 0; i < 3; i++)
	{
		vector_sum[0][i] = stol1[0][i] + stol2[0][i];
	}
}
// функция умножения матриц
void fnc_3(int (&matr_sum)[3][3],
           int matr1[3][3],
           int matr2[3][3])
{ 
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			for (int k = 0; k < 3; k++)
				matr_sum[i][j] = matr_sum[i][j] + matr1[i][k] * matr2[k][j];
		}
	}
}
// функция умножения матрицы на вектор
void fanc_4(int (&matr_sum)[3][3],int matr1[3][3],int vektor1[1][3])
  {
	for (int i = 0; i < 3; i++)
	for (int j = 0; j < 3; j++)
     {
	matr_sum[i][0] = matr_sum[i][0] + matr1[i][j] * vektor1[0][j];
     }
  }
//функция скалярного произведения
void fanc_5(int stol1[1][3], int stol2[1][3], int(&otvet))
{
	otvet = 0;
	for (int i = 0; i < 3; i++)
	{
		otvet = otvet + stol1[0][i] * stol2[0][i];
	}
}
//функция векторного произведения
void fanc_6(int (&vektor_sum)[1][3],int matrix_vektor[3][3],int vektor_stol1[1][3],int vektor_stol2[1][3])
{
	for (int i = 0; i < 3; i++) {
		if (i == 0) {
			for (int j = 0; j < 3; j++) {
				matrix_vektor[i][j] = 1; //присвоение элементам первой строки един векторы
			}

		}
		else {
			if (i == 1) {
				for (int j = 0; j < 3; j++) {
					matrix_vektor[i][j] = vektor_stol1[0][j];  //присвоение строке значение вектора
				}
			}
			else {
				for (int j = 0; j < 3; j++) {
					matrix_vektor[i][j] = vektor_stol2[0][j]; //присвоение cтроке значение вектора
				}
			}
		}
	}
	//обнуление вектора 
	for (int i = 0; i < 3; i++) {
		vektor_sum[0][i] = 0;

	}

	vektor_sum[0][0] = matrix_vektor[1][1] * matrix_vektor[2][2] -matrix_vektor[1][2] *matrix_vektor[2][1];
	vektor_sum[0][1] =matrix_vektor[1][0] *matrix_vektor[2][2] -matrix_vektor[1][2] *matrix_vektor[2][0];
	vektor_sum[0][2] =matrix_vektor[1][0] *matrix_vektor[2][1] -matrix_vektor[1][1] * matrix_vektor[2][0];
	 
}

//VIVOD_fun матрицы
void vyvod_matr(int matr_sum[3][3])
    {
for (int i = 0; i < 3; i++) {
for (int j = 0; j < 3; j++)
     {
std::cout << matr_sum[i][j] << " \t";
      }
std::cout << std::endl;
      }
std::cout << std::endl;
      }

//VIVOD_fun вектра
void vyvod_vect(int vector_sum[1][3])
{
for (int i = 0; i < 3; i++) {
std::cout << vector_sum[0][i] << " \t";
}
std::cout << std::endl;
}



int main()
{
	int matr1[3][3] = { { 1,2,3 },
	{ 4,5,6 },
	{ 7,8,9 } };

	int matr2[3][3] = { { 10,11,12 },
	{ 13,14,15 },
	{ 16,17,18 } };
	
	int matr_sum[3][3] = { { 0,0,0 },
	                       { 0,0,0 },
	                       { 0,0,0 } };

	int stol1[1][3] = { 1,
		                2,
		                3 };

	int stol2[1][3] = { 4,
		                5,
		                6 };

 
	int vektor_sum[1][3] = { 0,
		                     0,
		                     0 };
	int matrix_vektor[3][3] = { { 0,0,0 },
	{ 0,0,0 },
	{ 0,0,0 } };

	int otvet = 0;

  std::cout << "1 VIVOD_fun" << std::endl;
  fnc_1(matr1, matr2, matr_sum);
  vyvod_matr(matr_sum);
  
  std::cout << "2 VIVOD_fun" << std::endl;
  fnc_2(stol1,stol2,vektor_sum );
  vyvod_vect(vektor_sum);
  
  std::cout << "3 VIVOD_fun" << std::endl;
  fnc_3(matr_sum,matr1,matr2);
  vyvod_matr(matr_sum);
 
  std::cout << "4 VIVOD_fun" << std::endl;
  fanc_4(matr_sum,matr1,stol1);
  vyvod_matr(matr_sum);

  std::cout << "5 VIVOD_fun" << std::endl;
  fanc_5(stol1,stol2,otvet);
  std::cout << otvet << " \n";

  
  std::cout << "6 VIVOD_fun" << std::endl;
  fanc_6(vektor_sum, matrix_vektor,stol1,stol2);
  vyvod_vect(vektor_sum);
  
	


	system("pause");
	return 0;
}
// GIT
// консольное приложение с системой команд - пше
// позволяет создавать снимки текущего состояния файлов(версии)
// откатываться к предидущим,сливать версии вместе
// позволяет работать с удаленным хранилищем(репозиторием)
// является удобным средством командной работы
// 

