
// dinammas.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include <cmath>


void init_arr(int** &matr1, int** &matr2, int** &matr_sum, int stroki, int stolb)
{

	matr1 = new int*[stroki];
	matr2 = new int*[stroki];
	matr_sum = new int*[stroki];


	for (int i = 0; i < stroki; i++)
	{
		matr1[i] = new int[stolb];
		matr2[i] = new int[stolb];
		matr_sum[i] = new int[stolb];
	}
	std::cout << std::endl;
	std::cout << "matr1 =" << std::endl;
	for (int i = 0; i < stroki; i++)
	{
		for (int j = 0; j < stolb; j++)
		{
			matr1[i][j] = rand();
			std::cout << matr1[i][j] << "\t";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
	std::cout << "matr2 =" << std::endl;
	for (int i = 0; i < stroki; i++)
	{
		for (int j = 0; j < stolb; j++)
		{
			matr2[i][j] = rand();
			std::cout << matr2[i][j] << "\t";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;

}
//  Сложение матриц
void add_matr(int** &matr1, int** &matr2, int** &matr_sum, int stroki, int stolb)
{
	
	for (int i = 0; i < stroki; i++)
	{
		for (int j = 0; j < stolb; j++)
		{
			matr_sum[i][j] = 0;
			{
				matr_sum[i][j] = matr1[i][j] + matr2[i][j];
				std::cout << matr_sum[i][j] << '\t';//вывод результата сложения матриц
			}
		}
		std::cout << std::endl;
	}


	std::cout << std::endl;
}

void del_arr(int** &matr1, int** &matr2, int** &matr_sum, int stroki, int stolb)
{
	for (int i = 0; i < stroki; i++)
	{
		delete[] matr1[i];
		delete[] matr2[i];
		delete[] matr_sum[i];
	}
	delete[] matr1;
	delete[] matr2;
	delete[] matr_sum;

}

void init_vec(int** &vector1, int** &vector2, int** &vector_sum, int stroki, int stolb)
{

	vector1 = new int*[stroki];
	vector2 = new int*[stroki];
	vector_sum = new int*[stroki];


	for (int i = 0; i < stroki; i++)
	{
		vector1[i] = new int[stolb];
		vector2[i] = new int[stolb];
		vector_sum[i] = new int[stolb];
	}
	std::cout << std::endl;
	std::cout << "Vector A =" << std::endl;
	for (int i = 0; i < stroki; i++)
	{
		for (int j = 0; j < stolb; j++)
		{
			vector1[i][j] = rand();
			std::cout << vector1[i][j] << "\t";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
	std::cout << "Vector B =" << std::endl;
	for (int i = 0; i < stroki; i++)
	{
		for (int j = 0; j < stolb; j++)
		{
			vector2[i][j] = rand();
			std::cout << vector2[i][j] << "\t";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;

}

void add_vec(int** &vector1, int** &vector2, int** &vector_sum, int stroki, int stolb)
{
	std::cout << "Addition of vectors" << std::endl;
	for (int i = 0; i < stroki; i++)
	{
		for (int j = 0; j < stolb; j++)
		{
			vector_sum[i][j] = vector1[i][j] + vector2[i][j];
			std::cout << vector_sum[i][j] << "\t";
		}
		std::cout << std::endl;
		std::cout << std::endl;
	}
}

void del_vec(int** &vector1, int** &vector2, int** &vector_sum, int stroki, int stolb)
{
	for (int i = 0; i < stroki; i++)
	{
		delete[] vector1[i];
		delete[] vector2[i];
		delete[] vector_sum[i];
	}
	delete[] vector1;
	delete[] vector2;
	delete[] vector_sum;
}
// Умножение матриц
void mult_matr(int** &matr1, int** &matr2, int** &matr_sum, int stroki, int stolb)
{
	

	for (int i = 0; i < stroki; i++)
	{
		for (int j = 0; j < stolb; j++)
		{
			matr_sum[i][j] = 0;
			{
				for (int k = 0; k < stolb; k++)
					matr_sum[i][j] += matr1[i][k] * matr2[k][j];
			}
			std::cout << matr_sum[i][j] << "\t ";//вывод результата умножения матриц
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

void init_matr1nd_vec(int** &vector1, int** &matr2, int** &matr_sum, int stroki, int stroki_for_vec, int stolb)
{
	vector1 = new int*[stroki_for_vec];
	matr2 = new int*[stroki];
	matr_sum = new int*[stroki];


	for (int i = 0; i < stroki; i++)
	{
		matr2[i] = new int[stolb];
		matr_sum[i] = new int[stolb];
	}
	for (int i = 0; i < stroki_for_vec; i++)
	{
		vector1[i] = new int[stolb];
	}
	std::cout << std::endl;
	std::cout << "matr1 =" << std::endl;
	for (int i = 0; i < stroki_for_vec; i++)
	{
		for (int j = 0; j < stolb; j++)
		{
			vector1[i][j] = rand();
			std::cout << vector1[i][j] << "\t";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
	std::cout << "matr2 =" << std::endl;
	for (int i = 0; i < stroki; i++)
	{
		for (int j = 0; j < stolb; j++)
		{
			matr2[i][j] = rand();
			std::cout << matr2[i][j] << "\t";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}
// Умножение матрицы на вектор
void mult_matr1nd_vec(int** &vector1, int** &matr2, int** &matr_sum, int stroki, int stolb)
{
	

	for (int i = 0; i < stroki; i++)
	{
		for (int j = 0; j < stolb; j++)
		{
			matr_sum[i][j] = 0;
			{
				matr_sum[i][0] += matr2[i][j] * vector1[0][j];
			}
		}
		std::cout << matr_sum[i][0] << std::endl;//Вывод результата умножения матрицы на вектор
	}

	std::cout << std::endl;

}

void del_matr1nd_vec(int** &vector1, int** &matr2, int** &matr_sum, int stroki, int stroki_for_vec, int stolb)
{
	for (int i = 0; i < stroki; i++)
	{
		delete[] matr2[i];
		delete[] matr_sum[i];
	}
	for (int i = 0; i < stroki_for_vec; i++)
	{
		delete[] vector1[i];
	}

	delete[] vector1;
	delete[] matr2;
	delete[] matr_sum;
}
//Скалярное умножение векторов
void skal_mult_vec(int** &vector1, int** &vector2, int** &vector_sum, int stroki, int stolb)
{
	


	for (int i = 0; i < stroki; i++)
	{
		for (int j = 0; j < stolb; j++)
		{
			vector_sum[i][j] = 0;
			{
				vector_sum[i][0] += vector1[i][j] * vector2[i][j];
			}
		}
		std::cout << vector_sum[i][0] << std::endl;//Вывод результата скалярного умножения векторов
	}

	std::cout << std::endl;
}
//Векторное умножение векторов
void vec_mult_vec()
{
	
	int vector1[1][3];
	int vector2[1][3];
	std::cout << "Vector1 =" << std::endl;
	for (int i = 0; i < 1; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			vector1[i][j] = rand();
			std::cout << vector1[i][j] << "\t";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;

	std::cout << "Vector2 =" << std::endl;
	for (int i = 0; i < 1; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			vector2[i][j] = rand();
			std::cout << vector2[i][j] << "\t";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
	std::cout << "vector_sum = " << std::endl;
	int x = 1;
	int	y = 1;
	int	z = 1;//x, y, z, - единичные векторы
	int vector1xB[3][3] = { { x, y, z },//vector1xB - векторное произведение vector1 и vector2 в декартовой системе координат
	{ vector1[0][0], vector1[0][1], vector1[0][2] },
	{ vector2[0][0], vector2[0][1], vector2[0][2] } };
	int vector_sum[1][3];
	vector_sum[0][0] = vector1xB[1][1] * vector1xB[2][2] - vector1xB[1][2] * vector1xB[2][1];//1-ая координата вектора
	vector_sum[0][1] = vector1xB[1][2] * vector1xB[2][0] - vector1xB[1][0] * vector1xB[2][2];//2-ая координата вектора
	vector_sum[0][2] = vector1xB[1][0] * vector1xB[2][1] - vector1xB[1][1] * vector1xB[2][0];//3-ая координата вектора

	for (int i = 0; i < 1; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			std::cout << vector_sum[i][j] << "      ";//Вывод результата векторного умножение векторов
		}

	}

	std::cout << std::endl;
}

int main()
{
	int nstroki;
	int nstolb;
	int nstroki_for_vector = 1;
	int** matr1 = 0;
	int** matr2 = 0;
	int** matr_sum = 0;
	int** vector1 = 0;
	int** vector2 = 0;
	int** vector_sum = 0;
	int select_funct;

	std::cout << "1 - sloschenie mart" << std::endl;
	std::cout << "2 - sloschenie vector" << std::endl;
	std::cout << "3 - ymoshenie matr" << std::endl;
	std::cout << "4 - ymnoshenie matr na vector" << std::endl;
	std::cout << "5 - skal ymnoshenie vector" << std::endl;
	std::cout << "6 - vector proiz" << std::endl;
	std::cout << "Enter....";
	std::cin >> select_funct;

	switch (select_funct)
	{
		{
	case 1:
		std::cout << "sloschenie matr" << std::endl;
		std::cout << "vvedite razmer masiva" << std::endl;
		std::cout << "vvedite kolichestvo strok" << std::endl;
		std::cin >> nstroki;
		std::cout << "vvedite kolichestvo stolbsov" << std::endl;
		std::cin >> nstolb;
		init_arr(matr1, matr2, matr_sum, nstroki, nstolb);
		add_matr(matr1, matr2, matr_sum, nstroki, nstolb);
		del_arr(matr1, matr2, matr_sum, nstroki, nstolb);
		break;
		}
	case 2:
	{
		std::cout << "sloschenie vector" << std::endl;
		std::cout << "vvedite razmer vectora" << std::endl;
		std::cin >> nstolb;
		init_vec(vector1, vector2, vector_sum, nstroki_for_vector, nstolb);
		add_vec(vector1, vector2, vector_sum, nstroki_for_vector, nstolb);
		del_vec(vector1, vector2, vector_sum, nstroki_for_vector, nstolb);
		break;
	}
	case 3:
	{
		std::cout << "ymoshenie matr" << std::endl;
		std::cout << "vvedite razmer masiva" << std::endl;
		std::cout << "vvedite kolichestvo strok" << std::endl;
		std::cin >> nstroki;
		std::cout << "vvedite kolichestvo stolbsov" << std::endl;
		std::cin >> nstolb;
		init_arr(matr1, matr2, matr_sum, nstroki, nstolb);
		mult_matr(matr1, matr2, matr_sum, nstroki, nstolb);
		del_arr(matr1, matr2, matr_sum, nstroki, nstolb);
		break;
	}
	case 4:
	{
		std::cout << "ymnoshenie matr na vector" << std::endl;
		std::cout << "vvedite razmer masiva" << std::endl;
		std::cout << "vvedite kolichestvo strok" << std::endl;
		std::cin >> nstroki;
		std::cout << "vvedite kolichestvo stolbsov masiva and vectora" << std::endl;
		std::cin >> nstolb;
		init_matr1nd_vec(vector1, matr2, matr_sum, nstroki, nstroki_for_vector, nstolb);
		mult_matr1nd_vec(vector1, matr2, matr_sum, nstroki, nstolb);
		del_matr1nd_vec(vector1, matr2, matr_sum, nstroki, nstroki_for_vector, nstolb);
		break;
	}
	case 5:
	{
		std::cout << "skal ymnoshenie vector" << std::endl;
		std::cout << "vvedite razmer vectorov" << std::endl;
		std::cin >> nstolb;
		init_vec(vector1, vector2, vector_sum, nstroki_for_vector, nstolb);
		skal_mult_vec(vector1, vector2, vector_sum, nstroki_for_vector, nstolb);
		del_vec(vector1, vector2, vector_sum, nstroki_for_vector, nstolb);
		break;
	}
	case 6:
	{
		std::cout << "vector proiz" << std::endl;
		vec_mult_vec();

		break;
	}
	default:
	{
		std::cout << "oy shit, fatality error" << std::endl;
		break;
	}
	}


	system("pause");
	return 0;
}
