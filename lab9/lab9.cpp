// lab9.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include "weapon_class.h"
#include "launcher.h"
// выделили класс weapon_class в пару файлов h и cpp
// 1) для улучшения читаемости и удобства навигации в коде
// 2) для повышения модульности, повторной используемости кода

// синтаксис :
// class имя_нового класса : [тип_наследования] имя_родительского_класса1,
//	[тип_наследования] имя_родительского_класса2

// доступность свойств и методов базового класса внутри производного класса,
// даже если они не объявлены в производном

class assault_rifle : public weapon_class
{
public: // здесь public влияет на видимость снаружи функции assault_rifle
	assault_rifle()
		: weapon_class() // в начале конструктора assault_rifle вызывается конструктор базового класса
	{
		return;
	}
		double recoil; // сила отдачи
};
class soldier
{
public:
	soldier() {};
	int health;
	string name;
};
// множество наследования
class combat_unit : public weapon_class, public soldier
{
public:
	combat_unit()
	{

	}
};

int main()
{
	assault_rifle obj1;
	obj1.play_animation();
	combat_unit obj2;
	obj2.health = 15;
	obj2.load();


	system("pause");
	return 0;
}