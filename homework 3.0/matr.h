#pragma once
class MOSSIV
{

private:
	int ** matr_sum;
	int nstroki;
	int nstolb;

public:
	MOSSIV();
	MOSSIV(int stroki, int stolb);
	void init_arr(int ** &matr, int stroki, int stolb);
	void add_matr_or_vec(int ** &matr1, int ** &matr2, int stroki, int stolb);
	void mult_matr(int ** &matr1, int ** &matr2, int stroki_a, int stolb_a, int stolb_b);
	void mult_matr_and_vec(int ** &vector1, int ** &matr2, int stroki, int stolb);
	void vec_mult_vec();

	~MOSSIV();

};