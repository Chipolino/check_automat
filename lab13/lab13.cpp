// lab13.cpp: определяет точку входа для консольного приложения.
//


#include "stdafx.h"
#include <iostream>
#include <array>
#include <vector>
#include<list>

//STD-стандартныя библеотека языка C++
//функции printf? getchar, fopen, read - из данной библеотеки
// является связующим звеном 
//[C++] -> [STD] -> [OC]

//STL - standart template library(template - шаблон)
// это расшир STD, содерж структуры данных 
// и фун по работе с ними 
// именно из STL подкл потоки cout, cin, fstream, ifstream, ofstream, 
// контейнеры array, vecotr, list, map 
// итераторы 
// алгоритмы sort, copy и т.д.


//std::array 
//инкапсулирует массивы неизменной длины, является шаблоном 

//std::vector


//std::list


//std::map


int main()
{
	std::array<double, 10> double_array1; //объект, построенный из шаблона std::array<>, инкапсулирующий массив

	double_array1[5] = 10.5;
	double_array1.fill(0.0);
	int array_lenght = double_array1.size();
	//	double_array1.swap() - функция для обмена содержимым двух массивов 
	std::sort(double_array1.begin(), double_array1.end());
	std::array<double, 10> double_array2 = { 0 };
	std::copy(double_array1.begin(), double_array1.end(), double_array2.begin());



	//для созд двумерного массивов
	std::array<std::array<int, 3>, 3> mat33;

	//итератор, объект ссылающийся на элемент массива, вектора или списка
	//"улучшенный" вариант переменной-счётчика
	//нужен для связи STL
	std::array<double, 10>::iterator array_iterator;



	//std::vector
	//инкапсулирует одномерный массив и предоставляет средства 
	//для измерения его длинны
	//очень близким к std::vector по функционалу является std::string
	//ОСНОВНОЙ НЕДОСТАТОК
	//каждый раз при увеличении длинны vector заприашивает у ОС 
	//новую облать памяти и копирует туда старую (старую освобождает)
	//ОСНОВНОЕ ПРЕИМУЩЕСТВО
	//Доступ к элементам осуществяется напрямую по адресу
	//[адрес_1 + i]
	//ВЫВОД
	//Доступ быстрый, а вставка или увелечение медленные
	std::cout << "vector<> =";
	std::vector<long long> long_vector = { 50, 10, 20 };
	for (long long value : long_vector)
		std::cout << '\t' << value;
	std::cout << std::endl;

	long_vector.push_back(25);
	long_vector.push_back(30);

	std::cout << "vector<> after push =";
	for (long long value : long_vector)
		std::cout << '\t' << value;
	std::cout << std::endl;

	//присутсвует весь функционал std::array:
	//размер, сортировка, итераторы, копирование, обмен и т.д.

	std::sort(long_vector.begin(), long_vector.end());
	std::cout << "vector<> after sort() =";
	for (long long value : long_vector)
		std::cout << '\t' << value;
	std::cout << std::endl;

	long_vector[2] = 11;

	//вставкка в вектор
	long_vector.insert(long_vector.begin() + 2, 100500/*с помощью шаблона можно вставить несколько значений*/);
	std::cout << "vector<> after insert() =";
	for (long long value : long_vector)
		std::cout << '\t' << value;
	std::cout << std::endl;


	//std::list
	//Все наобарот: вставка и увиличение списка производится быстрее, чем в vector,
	//А доступ медленнее, чем дальше от 1-го элемента - тем МЕДЛЕННЕЕ
	//Так как для доступа к i-му элементу (определяли его адрес в ОП) необходимо
	//пройти по цепочке от 1-го элемента

   //в отличии от array и vector не является компактным массивом в памяти,
	//элементы std::list

	std::list<char> char_list = { 'b' };
	char_list.push_front('a');
	char_list.push_back('c');
	
	for (char ch : char_list)
	{
		std::cout << ch << std::endl;
	}
	class sample_class
	{
		int a;
		double b;

	};



	getchar();
	return 0;
}
